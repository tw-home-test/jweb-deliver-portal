import sys
from app import app
from optparse import OptionParser
## flask modules
from flask import send_file
from flask import render_template
from flask import redirect,send_from_directory
from flask import url_for

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.template.html')

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-p', '--port', dest='app_port', action='store')
    (options, args) = parser.parse_args()
    if options.app_port == None:
        print "Missing required argument: -p/--port"
        sys.exit(1)
    
    print (options.app_port)   
    app.run(host="0.0.0.0", port=int(options.app_port))
